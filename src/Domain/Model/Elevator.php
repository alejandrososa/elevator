<?php
namespace App\Domain\Model;

use \Exception;

class Elevator
{
    const STATE_DURATION = 3;
    const STATE_STANDING = 0;
    const STATE_MOVING_UP = 1;
    const STATE_MOVING_DOWN = -1;
    const STATE_DOOR_OPEN = 2;
    const STATE_ALARM = 3;
    const STATE_DOOR_CLOSE = 4;

    /** @var int */
    private $state = self::STATE_STANDING;

    /** @var array */
    private $queue = [];

    /** @var int */
    private $currentFloor;

    /** @var int */
    private $targetFloor;

    /** @var int */
    private $timestamp;

    public function __construct()
    {
        $this->currentFloor = 1;
        $this->resetTimeStamp();
    }

    /**
     * @return Elevator
     * @throws Exception
     */
    public function sendSignal($state)
    {
        if (!is_null($state)) {
            $this->state = $state;
            $this->resetTimeStamp();
        }

        return $this;
    }

    /**
     * @return Elevator
     * @throws Exception
     */
    public function addRequest(array $queue)
    {
        $this->queue = $queue;
        $this->targetFloor = array_shift($queue);
        return $this;
    }

    /**
     * @return Elevator
     * @throws Exception
     */
    public function process()
    {
        $this->checkAlarmState()
            ->checkDoor()
            ->getNextRequest();

        if ($this->hasCurrentRequest() && !$this->isDoorOpen()) {
            $this->moveElevator()
                ->checkIfRequestSatisfied();
        }

        return $this;
    }

    /** @return Elevator */
    private function checkIfRequestSatisfied()
    {
        if ($this->currentFloor === $this->targetFloor) {
            $this->state = self::STATE_DOOR_OPEN;
            $this->targetFloor = null;
            $this->updateTimestamp();
        }

        return $this;
    }

    /** @return Elevator */
    private function getNextRequest()
    {
        if (!$this->hasCurrentRequest() && !$this->isDoorOpen()) {
            $this->targetFloor = array_shift($this->queue);
            $this->resetTimeStamp();
        }

        return $this;
    }

    /** @return Elevator
     * @throws Exception
     */
    private function checkAlarmState()
    {
        if ($this->state === self::STATE_ALARM) {
            throw new Exception("Elevator is in alarm state. No requests will be handled.");
        }

        return $this;
    }

    /** @return Elevator */
    private function moveElevator()
    {
        switch (true) {
            case $this->targetFloor < $this->currentFloor:
                $this->state = self::STATE_MOVING_DOWN;
                $go = -1;
                break;

            case $this->targetFloor > $this->currentFloor:
                $this->state = self::STATE_MOVING_UP;
                $go = 1;
                break;
        }

        if ($this->isReadyForNextAction()) {
            $this->currentFloor += $go;
            $this->updateTimestamp();
        }

        return $this;
    }

    /** @return Elevator */
    private function checkDoor()
    {
        if ($this->isDoorOpen()) {
            if ($this->isReadyForNextAction()) {
                $this->state = self::STATE_STANDING;
                $this->updateTimestamp();
            }
        }

        return $this;
    }

    /** @return bool */
    private function isReadyForNextAction()
    {
        return ($this->timestamp + self::STATE_DURATION) <= time();
    }

    /** @return Elevator */
    private function updateTimestamp()
    {
        $this->timestamp += self::STATE_DURATION;
        return $this;
    }

    private function resetTimeStamp()
    {
        $this->timestamp = time();
        return $this;
    }

    /** @return int */
    public function getState()
    {
        return $this->state;
    }

    public function currentDescriptionState(): string
    {
        switch ($this->getState()) {
            case Elevator::STATE_ALARM:
                $description = "Elevator is between {$this->currentFloor()} and {$this->targetFloor()} in alarm state.";
                break;

            case Elevator::STATE_DOOR_OPEN:
                $description = "Door is open at {$this->currentFloor()} floor";
                break;

            case Elevator::STATE_STANDING:
                $description = "Elevator is standing at {$this->currentFloor()} floor";
                break;

            case Elevator::STATE_MOVING_DOWN:
                $description = "Elevator is moving down to {$this->targetFloor()} floor";
                break;

            case Elevator::STATE_MOVING_UP:
                $description = "Elevator is moving up to {$this->targetFloor()} floor";
                break;
        }

        return $description;
    }

    /** @return bool */
    public function hasCurrentRequest()
    {
        return !is_null($this->targetFloor);
    }

    /** @return bool */
    public function isDoorOpen()
    {
        return $this->state === self::STATE_DOOR_OPEN;
    }

    /** @return bool */
    public function isStanding()
    {
        return $this->state === self::STATE_STANDING;
    }

    /** @return int */
    public function currentFloor()
    {
        return $this->currentFloor;
    }

    /** @return int */
    public function targetFloor()
    {
        return $this->targetFloor;
    }

}
