<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 16/7/18 21:57
 */

namespace App\Domain\Model;


interface ElevatorRepository
{
    public function findElevator(): ?Elevator;
    public function save(Elevator $elevator): void;
    public function delete(): void;
}