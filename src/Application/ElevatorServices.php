<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 16/7/18 21:56
 */

namespace App\Application;


use App\Application\Contract\ApplicationService;
use App\Application\Service\FloorService;
use App\Application\Service\QueueService;
use App\Application\Service\SignalCollection;
use App\Domain\Model\ElevatorRepository;

abstract class ElevatorServices implements ApplicationService
{
    /**
     * @var ElevatorRepository
     */
    protected $repository;

    /**
     * @var FloorService
     */
    protected $floorService;

    /**
     * @var QueueService
     */
    protected $queueService;

    /**
     * @var SignalCollection
     */
    protected $signalCollection;


    public function __construct(
        ElevatorRepository $repository,
        FloorService $floorService,
        QueueService $queueService,
        SignalCollection $signalCollection
    )
    {
        $this->repository = $repository;
        $this->floorService = $floorService;
        $this->queueService = $queueService;
        $this->signalCollection = $signalCollection;
    }
}