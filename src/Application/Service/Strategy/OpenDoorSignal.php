<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 18/7/18 1:30
 */

namespace App\Application\Service\Strategy;


use App\Domain\Model\Elevator;
use Exception;

class OpenDoorSignal implements Signal
{
    /**
     * @param Elevator $elevator
     * @return string
     * @throws Exception
     */
    public function getState(Elevator $elevator): ?int
    {
        if (!$elevator->isDoorOpen() && !$elevator->isStanding()) {
            throw new Exception("Elevator is on it's way. Can not open door right now, sorry!");
        }

        return Elevator::STATE_DOOR_OPEN;
    }

    public function isMatch(int $signal): bool
    {
        return $signal === Elevator::STATE_DOOR_OPEN;
    }

    public function name(): string
    {
        return self::class;
    }
}