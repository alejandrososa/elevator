<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 18/7/18 1:30
 */

namespace App\Application\Service\Strategy;


use App\Domain\Model\Elevator;
use Exception;

class CloseDoorSignal implements Signal
{
    /**
     * @param Elevator $elevator
     * @return string
     * @throws Exception
     */
    public function getState(Elevator $elevator): ?int
    {
        $state = NULL;

        if ($elevator->isDoorOpen()) {
            $state = Elevator::STATE_DOOR_CLOSE;
        }

        return $state;
    }

    public function isMatch(int $signal): bool
    {
        return $signal === Elevator::STATE_DOOR_CLOSE;
    }

    public function name(): string
    {
        return self::class;
    }
}