<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 18/7/18 1:27
 */

namespace App\Application\Service\Strategy;


use App\Domain\Model\Elevator;

interface Signal
{
    public function getState(Elevator $elevator): ?int;
    public function isMatch(int $signal): bool;
    public function name(): string ;
}