<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 18/7/18 1:30
 */

namespace App\Application\Service\Strategy;


use App\Domain\Model\Elevator;
use Exception;

class AlarmSignal implements Signal
{
    /**
     * @param Elevator $elevator
     * @return string
     * @throws Exception
     */
    public function getState(Elevator $elevator): ?int
    {
        if ($elevator->isDoorOpen()) {
            throw new Exception("Elevator has door open, why should you push an alarm button?");
        }

        return Elevator::STATE_ALARM;
    }

    public function isMatch(int $signal): bool
    {
        return $signal === Elevator::STATE_ALARM;
    }

    public function name(): string
    {
        return self::class;
    }
}