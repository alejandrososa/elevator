<?php
namespace App\Application\Service;

use \Exception;

final class FloorService
{
    private $floorStates;

    public function __construct()
    {
        $this->floorStates = [
            1 => true,
            false,
            true,
            true,
            true,
        ];
    }

    /**
     * FloorService constructor.
     * @param array $floorStates
     * @throws Exception
     */
    public function setFloorStates(array $floorStates)
    {
        ksort($floorStates);
        $this->floorStates = $floorStates;
        $this->validateFloorStates();
    }

    /**
     * @throws Exception
     */
    private function validateFloorStates()
    {
        $i = 1;
        foreach ($this->floorStates as $floor => $floorState) {
            if ($floor !== $i++) {
                throw new Exception("Inconsistent floor states input.");
            }

            if (!is_bool($floorState)) {
                throw new Exception("Floor status must be bool. Received: " . PHP_EOL . print_r($floorState, true));
            }
        }
    }

    /**
     * @param $floor
     * @return mixed
     * @throws Exception
     */
    public function floorState($floor)
    {
        if (!in_array($floor, $this->floorStates)) {
            throw new Exception("Unknown floor status requested: {$floor}");
        }

        return $this->floorStates[$floor];
    }

    public function floorStatus()
    {
        return $this->floorStates;
    }

}
