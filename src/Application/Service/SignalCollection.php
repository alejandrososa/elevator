<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 18/7/18 2:09
 */

namespace App\Application\Service;


use App\Application\Service\Strategy\Signal;

class SignalCollection implements \Countable, \Iterator
{
    /**
     * @var Signal[]
     */
    private $signals = [];

    /**
     * @var int
     */
    private $currentIndex = 0;

    public function addSignal(Signal $signal)
    {
        $this->signals[] = $signal;
    }

    public function removeSignal(Signal $signalToRemove)
    {
        foreach ($this->signals as $key => $signal) {
            if ($signal->name() === $signalToRemove->name()) {
                unset($this->signals[$key]);
            }
        }

        $this->signals = array_values($this->signals);
    }

    public function count(): int
    {
        return count($this->signals);
    }

    public function current(): Signal
    {
        return $this->signals[$this->currentIndex];
    }

    public function key(): int
    {
        return $this->currentIndex;
    }

    public function next()
    {
        $this->currentIndex++;
    }

    public function rewind()
    {
        $this->currentIndex = 0;
    }

    public function valid(): bool
    {
        return isset($this->signals[$this->currentIndex]);
    }
}