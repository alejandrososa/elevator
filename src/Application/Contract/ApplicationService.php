<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 16/7/18 22:49
 */

namespace App\Application\Contract;

interface ApplicationService
{
    public function execute($request = null);
}