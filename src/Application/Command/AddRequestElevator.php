<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 17/7/18 1:52
 */

namespace App\Application\Command;


use App\Application\ElevatorServices;
use App\Domain\Model\Elevator;
use Exception;

final class AddRequestElevator extends ElevatorServices
{

    private $queue = [];

    /**
     * @param ElevatorRequest $request
     * @return Elevator|null
     * @throws Exception
     */
    public function execute($request = null): ?Elevator
    {
        $elevator = $this->repository->findElevator();
        $elevator->process();

        try {

            if (!$this->floorService->floorState($request->sourceFloor())) {
                throw new Exception("Source floor is under maintenance.");
            }

            if (!$this->floorService->floorState($request->targetFloor())) {
                throw new Exception("Target floor is under maintenance.");
            }

            $this->queue = $this->queueService->addRequest($this->queue, $elevator, $request);
            $elevator->addRequest($this->queue);
            $this->repository->save($elevator);
        } catch (Exception $e) {

        }
        return $elevator;
    }
}