<?php
namespace App\Application\Command;

use \Exception;

final class SignalRequest
{
    private $signal;

    /**
     * SignalRequest constructor.
     * @param $signal
     * @throws Exception
     */
    public function __construct($signal)
    {
        if (!is_int($signal)) {
            throw new Exception("Invalid signal value: Integer expected, got: " . print_r($signal));
        }

        $this->signal = $signal;
    }

    public function signal()
    {
        return $this->signal;
    }
}
