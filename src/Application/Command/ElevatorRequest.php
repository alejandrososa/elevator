<?php
namespace App\Application\Command;

use \Exception;

final class ElevatorRequest
{
    private $sourceFloor;
    private $targetFloor;

    /**
     * ElevatorRequest constructor.
     * @param $source
     * @param $target
     * @throws Exception
     */
    public function __construct($source, $target)
    {
        if (!is_int($source)) {
            throw new Exception("Invalid source floor value: Integer expected, got: " . print_r($source));
        }

        if (!is_int($target)) {
            throw new Exception("Invalid target floor value: Integer expected, got: " . print_r($target));
        }

        $this->sourceFloor = $source;
        $this->targetFloor = $target;
    }

    public function sourceFloor()
    {
        return $this->sourceFloor;
    }

    public function targetFloor()
    {
        return $this->targetFloor;
    }

}
