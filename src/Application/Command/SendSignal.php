<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 17/7/18 1:52
 */

namespace App\Application\Command;


use App\Application\ElevatorServices;
use App\Domain\Model\Elevator;
use Exception;

final class SendSignal extends ElevatorServices
{
    /**
     * @param SignalRequest $request
     * @return Elevator|null
     * @throws Exception
     */
    public function execute($request = null): void
    {
        $elevator = $this->repository->findElevator();

        $elevator->process();
        $state = NULL;

        foreach ($this->signalCollection as $signal) {
            if ($signal->isMatch($request->signal())) {
                $state = $signal->getState($elevator);
            }
        }

        $elevator->sendSignal($state);
        $this->repository->save($elevator);
    }
}