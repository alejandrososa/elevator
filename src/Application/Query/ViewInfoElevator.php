<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 16/7/18 22:48
 */

namespace App\Application\Query;


use App\Application\ElevatorServices;
use App\Domain\Model\Elevator;
use Exception;

class ViewInfoElevator extends ElevatorServices
{
    public function execute($request = null): ?Elevator
    {
        $elevator = $this->repository->findElevator();

        try {
            $elevator->process();
            $this->repository->save($elevator);
        } catch (Exception $e) {

        }
        return $elevator;
    }
}