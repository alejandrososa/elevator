<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 16/7/18 22:48
 */

namespace App\Application\Query;


use App\Application\ElevatorServices;
use Exception;

class ViewFloorsStatus extends ElevatorServices
{
    /**
     * @param null $request
     * @return array|null
     * @throws Exception
     */
    public function execute($request = null): ?array
    {
        try {
            $status = $this->floorService->floorStatus();
        } catch (Exception $e) {

        }
        return $status;
    }
}