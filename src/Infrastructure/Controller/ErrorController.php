<?php namespace App\Infrastructure\Controller;

use \Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ErrorController extends AbstractController
{
    public function exceptionAction(Exception $e)
    {
        http_response_code($e->getCode());
        return $this->json(["error" => $e->getMessage()]);
    }

}
