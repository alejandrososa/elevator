<?php namespace App\Infrastructure\Controller;

use App\Application\Query\ViewFloorsStatus;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FloorsController extends AbstractController
{
    /**
     * @var ViewFloorsStatus
     */
    private $query;

    public function __construct(ViewFloorsStatus $query)
    {
        $this->query = $query;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getFloorsAction()
    {
        $floors = $this->query->execute();

        return $this->json(["floorsCount" => count($floors), "floorsStatus" => $floors]);
    }

}
