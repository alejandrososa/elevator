<?php namespace App\Infrastructure\Controller;

use App\Application\Command\AddRequestElevator;
use App\Application\Command\ElevatorRequest;
use App\Application\Command\ResetElevator;
use App\Application\Exception\BadRequestException;
use App\Application\Query\ViewInfoElevator;
use Symfony\Component\HttpFoundation\Request;

class ElevatorController extends BaseController
{
    /**
     * @var ViewInfoElevator
     */
    private $query;

    /**
     * @var AddRequestElevator
     */
    private $commandAddRequest;

    /**
     * @var ResetElevator
     */
    private $commandReset;

    public function __construct(
        ViewInfoElevator $query,
        AddRequestElevator $commandAddRequest,
        ResetElevator $commandReset)
    {
        $this->query = $query;
        $this->commandAddRequest = $commandAddRequest;
        $this->commandReset = $commandReset;
    }

    public function getInfoAction()
    {
        $elevator = $this->query->execute();
        return $this->json([
            "description" => $elevator->currentDescriptionState(),
            "currentFloor" => $elevator->currentFloor(),
            "targetFloor" => $elevator->targetFloor(),
            "state" => $elevator->getState()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function addRequestAction(Request $request)
    {
        $content = $request->getContent();
        if (!isset($content)) {
            throw new BadRequestException("Signal not given");
        }

        $content = json_decode($content);

        $this->commandAddRequest->execute(new ElevatorRequest($content->from, $content->to));
        return $this->json(["success" => "Request successfully received."]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function resetStateAction(Request $request)
    {
        $this->commandReset->execute();
        return $this->json(["success" => "State was successfully reset."]);
    }

}
