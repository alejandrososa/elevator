<?php
/**
 * elevators, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 17/7/18 2:32
 */

namespace App\Infrastructure\Controller;


use App\Application\Service\SignalCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function homepageAction()
    {
        return $this->render('homepage.html.twig',[]);
    }
}