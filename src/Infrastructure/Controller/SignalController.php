<?php namespace App\Infrastructure\Controller;

use App\Application\Command\SendSignal;
use App\Application\Command\SignalRequest;
use App\Application\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

class SignalController extends BaseController
{
    /**
     * @var SendSignal
     */
    private $command;

    public function __construct(SendSignal $command)
    {
        $this->command = $command;
    }

    /**
     * @param Request $request
     * @return string
     * @throws BadRequestException
     * @throws \Exception
     */
    public function sendSignalAction(Request $request)
    {
        $content = $request->getContent();
        if (!isset($content)) {
            throw new BadRequestException("Signal not given");
        }

        $content = json_decode($content);
        $commanRequest = new SignalRequest($content->signal);
        $this->command->execute($commanRequest);

        return $this->json(["success" => "Signal successfully handled."]);
    }
}
