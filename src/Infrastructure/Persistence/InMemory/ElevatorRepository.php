<?php

namespace App\Infrastructure\Persistence\InMemory;

use App\Domain\Model\Elevator;
use App\Domain\Model\ElevatorRepository as Repository;
use Symfony\Component\Cache\Simple\FilesystemCache;

final class ElevatorRepository implements Repository
{
    private const ENTITY = 'elevator.entity';

    /**
     * @var FilesystemCache
     */
    private $cache;

    public function __construct(FilesystemCache $cache)
    {
        $this->cache = $cache;
    }
//
//    /** @return FloorService */
//    public function getFloorsService()
//    {
//        if (is_null($this->floorService)) {
//            $this->floorService = new FloorService([
//                1 => true,
//                false,
//                true,
//                false,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true,
//                true
//            ]);
//        }
//        return $this->floorService;
//    }
//
//    /** @return Elevator */
//    public function getElevator()
//    {
//        $elevator = $this->restoreElevatorStatus();
//
//        if ($elevator === FALSE) {
//            $floorService = $this->getFloorsService();
//            $queueService = new QueueService();
//            $signalProcessor = new AlarmSignalProcessor(new DoorOpenSignalProcessor(new DoorCloseSignalProcessor));
//            $elevator = new Elevator($queueService, $floorService, $signalProcessor);
//        }
//
//        return $elevator;
//    }
//
//    private function restoreElevatorStatus()
//    {
//        $data = file_get_contents($this->file);
//        $elevator = @unserialize($data);
//        return $elevator;
//    }
//
//    public function saveElevator(Elevator $elevator)
//    {
//        file_put_contents($this->file, serialize($elevator));
//    }

    /**
     * @return Elevator|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function findElevator(): ?Elevator
    {
        if ($this->cache->has(self::ENTITY)) {
            $data = $this->cache->get(self::ENTITY);
            $elevator = @unserialize($data);
        } else {
            $elevator = new Elevator();
        }

        return $elevator;
    }

    /**
     * @param Elevator $elevator
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function save(Elevator $elevator): void
    {
        $this->cache->set(self::ENTITY, serialize($elevator));
    }

    public function delete(): void
    {
        if ($this->cache->has(self::ENTITY)) {
            $this->cache->delete(self::ENTITY);
        }
    }
}
