Elevator Test
======

Simple test using hexagonal architecture DDD, PHP 7.1, Symfony 4, and Angular.


**Installation steps**:

    composer install && php bin/console server:run
    

Go to the browser in

     http://127.0.0.1:8000   

 